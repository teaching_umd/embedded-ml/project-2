import matplotlib.pyplot as plt
import tensorflow as tf
from keras.callbacks import EarlyStopping, ModelCheckpoint
import larq as lq

(train_images, train_labels), (test_images, test_labels) = tf.keras.datasets.mnist.load_data()

train_images = train_images.reshape((60000, 784, 1))
test_images = test_images.reshape((10000, 784, 1))

# Normalize pixel values to be between -1 and 1
train_images, test_images = train_images / 255, test_images / 255


kwargs = dict(#input_quantizer="ste_heaviside",
              kernel_quantizer="ste_heaviside",
              kernel_constraint="weight_clip")
callbacks = [EarlyStopping(monitor='val_loss', patience=8)]  # uses validation set to stop training when it start overfitting

model1 = tf.keras.models.Sequential()
model1.add(tf.keras.layers.Conv1D(4, 2, activation='relu', kernel_initializer='he_uniform', input_shape=(784, 1)))
model1.add(tf.keras.layers.Flatten())
model1.add(tf.keras.layers.Dense(10, activation='softmax'))
model = tf.keras.models.Sequential()
model.add(lq.layers.QuantConv1D(4, 2,
                                 #input_quantizer="ste_heaviside",
                                 kernel_quantizer="ste_heaviside",
                                 kernel_constraint="weight_clip",
                                 use_bias=False,
                                 input_shape=(784, 1)))

model.add(tf.keras.layers.Flatten())
model.add(lq.layers.QuantDense(10, use_bias=False, activation='softmax', **kwargs))

#lq.models.summary(model)
print(model1.summary())
model1.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

history_1 = model1.fit(train_images, train_labels, batch_size=128, epochs=15)
metrics_1 = history_1.history
test_loss, test_acc = model1.evaluate(test_images, test_labels)
print(f"Test accuracy {test_acc * 100:.2f} %")



history = model.fit(train_images, train_labels, batch_size=128, epochs=15)
metrics = history.history
test_loss, test_acc = model.evaluate(test_images, test_labels)
print(f"Test accuracy {test_acc * 100:.2f} %")




plt.plot(history_1.epoch, metrics_1['loss'], metrics['loss'])
plt.legend(['loss', 'quant_loss'])
plt.show()
plt.plot(history_1.epoch, metrics_1['accuracy'],  metrics['accuracy'])
plt.legend(['accuracy', 'quant_accuracy'])
plt.show()



# with lq.context.quantized_scope(True):
#     Layer1_weights = model.get_weights()[0]
#
#     Layer1_bias = model.get_weights()[1]
#     label_predict = np.argmax(model.predict(test_images, batch_size=1), axis=0)
#
#     Model_para_inputs = {"Layer1_weights": Layer1_weights, "Layer1_bias": Layer1_bias, "image_test": test_images,
#                          "label_test": test_labels, "label_predict": label_predict}
#     sio.savemat('./Model_para_inputs.mat', Model_para_inputs)
#
#
#
