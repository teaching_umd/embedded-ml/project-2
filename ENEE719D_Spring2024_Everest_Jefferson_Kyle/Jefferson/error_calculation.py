# -*- coding: utf-8 -*-
"""
Created on Mon May 13 09:36:08 2024

@author: jeder
"""

import numpy as np

# Load the data from files
weights_rp = np.loadtxt('weightsrp.txt')
weights_rn = np.loadtxt('weightsrn.txt')
weights_ip = np.loadtxt('weightsip.txt')
weights_in = np.loadtxt('weightsin.txt')
inputs_re = np.loadtxt('inputsre.txt')
inputs_im = np.loadtxt('inputsim.txt')

inputs_re = np.where(inputs_re > 7, inputs_re - 8, inputs_re)
inputs_im = np.where(inputs_im > 7, inputs_im - 8, inputs_im)

bias = (weights_rp[-1] - weights_rn[-1]) + 1j * (weights_ip[-1] - weights_in[-1])
weights_rp = weights_rp[:-1]
weights_rn = weights_rn[:-1]
weights_ip = weights_ip[:-1]
weights_in = weights_in[:-1]

weights = (weights_rp - weights_rn) + 1j * (weights_ip - weights_in)
inputs = inputs_re + 1j * inputs_im

results = weights * inputs

results += bias

MAC_output_IM = np.loadtxt('MAC_output_IM.txt')
MAC_output_RE = np.loadtxt('MAC_output_RE.txt')
MAC_output = MAC_output_RE + 1j * MAC_output_IM

time_domain_model = np.fft.ifft(results)
time_domain_circuit = np.fft.ifft(MAC_output)

time_domain_model = results
time_domain_circuit = MAC_output

relative_errors = np.abs(time_domain_model - time_domain_circuit) / (np.abs(time_domain_model) + 1e-10)  # Adding a small constant to avoid division by zero
average_relative_error = np.mean(relative_errors) * 100  # Convert to percentage

# Print results
print("Average error between the model and the circuit outputs in the time domain (percentage):", average_relative_error)
