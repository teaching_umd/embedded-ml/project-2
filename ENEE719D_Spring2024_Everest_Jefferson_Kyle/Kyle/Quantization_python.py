# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 16:50:51 2021

@author: sshah389
"""

#This file takes in the real value and quantize and creates a text file to input the values into cadence.
#Here the code is doing fixed point quantization


import scipy.io as sio
import numpy as np
import matplotlib.pyplot as pyplot
from scipy.fft import fft, ifft


def real_to_binary(weights,Q=11,R=4):
    d1=weights
    dim_array=d1.ndim
    if dim_array>1:
        [row_size,column_size]=d1.shape
    else:
        row_size=d1.shape
        d1=d1.reshape([row_size[0],1])
        [row_size,column_size]=d1.shape
    Q1=[["0"]]
    for j_loop in range(0,row_size-1):
        Q1.append(["0"])
    for j_loop in range(0,row_size):
        for i_loop in range(0,column_size-1):
                Q1[j_loop].append("0")
    integ_d1=np.floor(abs(d1))
    fract_d1=abs(d1)-integ_d1
    for j_loop in range(0,row_size):
        for i_loop in range(0,column_size):
            if d1[j_loop,i_loop]<0:
                Q1[j_loop][i_loop]='1'
            if d1[j_loop,i_loop]<=(2**R-1):
                Q1[j_loop][i_loop]+=np.binary_repr(int(integ_d1[j_loop,i_loop]),width=R)
                Q1[j_loop][i_loop]+=np.binary_repr(int(fract_d1[j_loop,i_loop]*2**Q),width=Q)
            else:
                Q1[j_loop][i_loop]+=np.binary_repr(int((2**R)-1),width=R)
                Q1[j_loop][i_loop]+=np.binary_repr(int((2**Q)-1),width=Q)              
    return Q1
            
                


def binary_to_real(weights_bin,Q=11,R=4):
    Q1=weights_bin
    row_size=len(Q1)
    column_size=len(Q1[0])
    if column_size>1:
        d1=np.zeros([row_size,column_size])
    else:
        d1=np.zeros([row_size,1])
    for j_loop in range(0,row_size):
        for i_loop in range(0,column_size):
            if Q1[j_loop][i_loop][0]=="0":
                sign_bit=1
            else:
                sign_bit=-1
            decimal_value=0    
            for decimcal_loop in range(Q+R,0,-1):
               if Q1[j_loop][i_loop][(Q+R)-decimcal_loop+1]=='1':
                   decimal_value=decimal_value+2**(decimcal_loop-Q-1)
               else:
                   decimal_value=decimal_value+0       
            d1[j_loop,i_loop]=sign_bit*decimal_value
    if column_size==1:
        d1=d1.reshape([row_size])        
    return d1

    
def real_to_decimal(weights,Q=11,R=4):
    d1=weights
    dim_array=d1.ndim
    if dim_array==1:
        row_size=d1.shape
        d1=d1.reshape([row_size[0],1])
    [row_size,column_size]=d1.shape
    decimal=np.zeros([row_size,column_size])
    Q1=[['0' for i in range(column_size)] for j in range(row_size)]
    integ_d1=np.floor(abs(d1))
    fract_d1=abs(d1)-integ_d1
    for j_loop in range(0,row_size):
        for i_loop in range(0,column_size):
            if d1[j_loop,i_loop]<0:
                Q1[j_loop][i_loop]='1'
            if d1[j_loop,i_loop]<=(2**R-1):
                Q1[j_loop][i_loop]+=np.binary_repr(int(integ_d1[j_loop,i_loop]),width=R)
                Q1[j_loop][i_loop]+=np.binary_repr(int(fract_d1[j_loop,i_loop]*2**Q),width=Q)
            else:
                Q1[j_loop][i_loop]+=np.binary_repr(int((2**R)-1),width=R)
                Q1[j_loop][i_loop]+=np.binary_repr(int((2**Q)-1),width=Q) 
            decimal[j_loop,i_loop]=int(Q1[j_loop][i_loop],base=2)                  
    return decimal

def decimal_to_real(weights_bin,Q=11,R=4):
    #make sure weights_bin has 2 dimn with the second dim as 1. 
    row_size,column_size=weights_bin.shape
    Q1=[]
    for i_loop in range(0,row_size):
            Q1.append(np.binary_repr(weights_bin[i_loop,0],width=Q+R+1))
    d1=np.zeros([row_size,1])
    for j_loop in range(0,row_size):
        if Q1[j_loop][0]=="0":
            sign_bit=1
        else:
            sign_bit=-1
        decimal_value=0    
        for decimcal_loop in range(Q+R,0,-1):
           if Q1[j_loop][(Q+R)-decimcal_loop+1]=='1':
               decimal_value=decimal_value+2**(decimcal_loop-Q-1)
           else:
               decimal_value=decimal_value+0       
        d1[j_loop,0]=sign_bit*decimal_value      
    return d1


Model_para_inputs=sio.loadmat('Model_para_inputs.mat')
Layer1_weights=Model_para_inputs['Layer1_weights']
Layer1_bias=Model_para_inputs['Layer1_bias']
#For the demonstrating the use I am only quantizing weights and biases from only the first neuron



input_test=Model_para_inputs['image_test']
input_test=np.reshape(input_test, [10000, 784])

input_test_decimal=np.reshape(real_to_decimal(binary_to_real(real_to_binary(np.concatenate([input_test[0,:],np.array([1.0])])))),[-1])
#Note there is a 1 added at the end of the input to enable multiplication of the bias with an input of 1. 
input_filere = open("inputsre16.txt", "w")
input_fileim = open("inputsim16.txt", "w")
'''
a=np.where(input_test_decimal!=0)
input_non_zero=input_test_decimal[a]
'''

ip_re=fft(np.concatenate([input_test[0,:],np.array([1.0])])).real
ip_im=fft(np.concatenate([input_test[0,:],np.array([1.0])])).imag

fftipre_decimal=np.reshape(real_to_decimal(binary_to_real(real_to_binary(ip_re))),[-1])
fftipim_decimal=np.reshape(real_to_decimal(binary_to_real(real_to_binary(ip_im))),[-1])


np.savetxt(input_filere, fftipre_decimal,newline='\n',fmt='%d')
np.savetxt(input_fileim, fftipim_decimal,newline='\n',fmt='%d')
input_filere.close()
input_fileim.close()



Neuron1_weights=np.reshape(Layer1_weights[:,:,0,:], 9)


Neuron1_weights_quant_re=binary_to_real(real_to_binary(fft(Neuron1_weights).real))
Neuron1_weights_quant_im=binary_to_real(real_to_binary(fft(Neuron1_weights).imag))
Neuron1_weights_quant_rp=np.zeros([9])
Neuron1_weights_quant_rn=np.zeros([9])
Neuron1_weights_quant_ip=np.zeros([9])
Neuron1_weights_quant_in=np.zeros([9])
# split pos neg
for i in range (9):
    if Neuron1_weights_quant_re[i]<0:
        Neuron1_weights_quant_rn[i]=-Neuron1_weights_quant_re[i]
    else:
        Neuron1_weights_quant_rp[i]=Neuron1_weights_quant_re[i]
    if Neuron1_weights_quant_im[i]<0:
        Neuron1_weights_quant_in[i]=-Neuron1_weights_quant_im[i]
    else:
        Neuron1_weights_quant_ip[i]=Neuron1_weights_quant_im[i]

Neuron1_weights_quant_decimal_rp=real_to_decimal(Neuron1_weights_quant_rp)
Neuron1_weights_quant_decimal_rn=real_to_decimal(Neuron1_weights_quant_rn)
Neuron1_weights_quant_decimal_ip=real_to_decimal(Neuron1_weights_quant_ip)
Neuron1_weights_quant_decimal_in=real_to_decimal(Neuron1_weights_quant_in)
#checking the quantization error
#Quantization_error=Neuron1_weights - Neuron1_weights_quant
#pyplot.plot(Quantization_error)


Neuron1_bias=Layer1_bias[0]

    
Neuron1_bias_quant_re=binary_to_real(real_to_binary(fft(Neuron1_bias).real))
Neuron1_bias_quant_im=binary_to_real(real_to_binary(fft(Neuron1_bias).imag))
Neuron1_bias_quant_rp=np.zeros([1])
Neuron1_bias_quant_rn=np.zeros([1])
Neuron1_bias_quant_ip=np.zeros([1])
Neuron1_bias_quant_in=np.zeros([1])
if Neuron1_bias_quant_re<0:
    Neuron1_bias_quant_rn=-Neuron1_bias_quant_re
else:
    Neuron1_bias_quant_rp=Neuron1_bias_quant_re
if Neuron1_bias_quant_im<0:
    Neuron1_bias_quant_in=-Neuron1_bias_quant_im
else:
    Neuron1_bias_quant_ip=Neuron1_bias_quant_im
Neuron1_bias_quant_decimal_rp=real_to_decimal(Neuron1_bias_quant_rp)
Neuron1_bias_quant_decimal_rn=real_to_decimal(Neuron1_bias_quant_rn)
Neuron1_bias_quant_decimal_ip=real_to_decimal(Neuron1_bias_quant_ip)
Neuron1_bias_quant_decimal_in=real_to_decimal(Neuron1_bias_quant_in)



parameters_rp=np.concatenate([np.reshape(Neuron1_weights_quant_decimal_rp,[-1]),Neuron1_bias_quant_decimal_rp[0]])
parameters_rn=np.concatenate([np.reshape(Neuron1_weights_quant_decimal_rn,[-1]),Neuron1_bias_quant_decimal_rn[0]])
parameters_ip=np.concatenate([np.reshape(Neuron1_weights_quant_decimal_ip,[-1]),Neuron1_bias_quant_decimal_ip[0]])
parameters_in=np.concatenate([np.reshape(Neuron1_weights_quant_decimal_in,[-1]),Neuron1_bias_quant_decimal_in[0]])






#parameters_nonzero=parameters[a]
#10 class
weights_file_rp = open("weightsrp16.txt", "w")
np.savetxt(weights_file_rp, parameters_rp,newline='\n',fmt='%d')
weights_file_rp.close()

weights_file_rn = open("weightsrn16.txt", "w")
np.savetxt(weights_file_rn, parameters_rn,newline='\n',fmt='%d')
weights_file_rn.close()

weights_file_ip = open("weightsip16.txt", "w")
np.savetxt(weights_file_ip, parameters_ip,newline='\n',fmt='%d')
weights_file_ip.close()

weights_file_in = open("weightsin16.txt", "w")
np.savetxt(weights_file_in, parameters_in,newline='\n',fmt='%d')
weights_file_in.close()



