Everest Bloomer
Jefferson Ederhion
Kyle Wu

1R ReRam Qmac
Latency: 2 E-10 S
Power: 7.73 E-5 J (Per Qmac)
Accuracy: 13.4046% (For 1 Convolution Layer) (Discussion of Accuracy in Slides)

**How was the Accuracy tested**
The ReRam device was implementing using VerilogA, and a simplifed version was used to upload the weights directly without needing a write cycle.
Then, the model was quantized to 4 bits (1 sign, 1 predecimal, 2 postdecimal).
This 4 bit model output was compared between the ReRam VerilogA implementation, and doing the multiplication directly in Python.
That gives the accuracy comparison above.

Seperately, quantization error was calculated between the 4 bit model and a 16 bit model.
The quantization error is: 100%
The comparison between the 4 bit model and the 16 bit model is non-favorable