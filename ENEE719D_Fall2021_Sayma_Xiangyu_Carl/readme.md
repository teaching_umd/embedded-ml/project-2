ENEE719D Fall 2021
Project2 ReRAM IMC MAC
Group members: Sayma Chowdhury, Carl Brando, Xiangyu Mao
12/9/2021

Directories & files included:

-VerilogA: dir with all codes
----array.va: resistor array
----dac.va: DAC for input
----sense_amp.va: sense-amplifier for current sensing
----ReRAM_IMC_test_full: testing setup for 10 neurons (please import all VerilogA files into the VerilogA library)

-Input_files: inputs & weights for all 10 neurons, 4~8bit quantized

-ENEE719D_Project2_Report.pdf/doc: project report

-ENEE719D_Project2_Sayma_Brando_Mao.pptx: project slides

-output.xlsx: data collected & graphs