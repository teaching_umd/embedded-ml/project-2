# -*- coding: utf-8 -*-
"""
Created on Thu May  9 17:03:00 2024

@author: student
"""

output_file = open("weightsreal.txt", "r")
file1 = open("weightspos.txt" , "w")
file2 = open("weightsneg.txt" , "w")

while True:

    line = output_file.readline()
    if not line:
        break
    if int(line) <=15:
        file1.write(line)
        file2.write("15\n")
    else:
        x = str(15-(int(line)-16))
        x = x + "\n"
        file2.write(x)
        file1.write("0\n")
    # if line is empty
    # end of file is reached
output_file.close()
file1.close()
file2.close()