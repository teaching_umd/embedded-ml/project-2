# -*- coding: utf-8 -*-
"""
Created on Thu May  9 17:03:00 2024

@author: student
"""

w_file = open("weightsreal_2.txt", "r")
i_file = open("inputsreal_2.txt", "r")
sum = 0

while True:

    weight = float(w_file.readline())
    input = float(i_file.readline())
    if (not(weight)) or (not(input)):
        break
    sum = sum+weight * input
    print(sum)
w_file.close()
i_file.close()
print(sum)