import scipy.io as sio
import numpy as np
import matplotlib.pyplot as pyplot


def real_to_binary(weights, Q=4, R=0):
    d1 = weights
    dim_array = d1.ndim
    if dim_array > 1:
        [row_size, column_size] = d1.shape
    else:
        row_size = d1.shape
        d1 = d1.reshape([row_size[0], 1])
        [row_size, column_size] = d1.shape
    Q1 = [["0"]]
    for j_loop in range(0, row_size - 1):
        Q1.append(["0"])
    for j_loop in range(0, row_size):
        for i_loop in range(0, column_size - 1):
            Q1[j_loop].append("0")
    integ_d1 = np.floor(abs(d1))
    fract_d1 = abs(d1) - integ_d1
    for j_loop in range(0, row_size):
        for i_loop in range(0, column_size):
            if d1[j_loop, i_loop] < 0:
                Q1[j_loop][i_loop] = "1"
            if d1[j_loop, i_loop] <= (1):
                Q1[j_loop][i_loop] += np.binary_repr(
                    int(fract_d1[j_loop, i_loop] * 2**Q), width=Q
                )

            else:
                print(
                    "1 converted to 0.", np.binary_repr(int(0.875 * 2**Q), width=R + Q)
                )
                Q1[j_loop][i_loop] += np.binary_repr(int(0.875 * 2**Q), width=R + Q)

    return Q1


def binary_to_real(weights_bin, Q=4, R=0):
    Q1 = weights_bin
    row_size = len(Q1)
    column_size = len(Q1[0])
    if column_size > 1:
        d1 = np.zeros([row_size, column_size])
    else:
        d1 = np.zeros([row_size, 1])
    for j_loop in range(0, row_size):
        for i_loop in range(0, column_size):
            if Q1[j_loop][i_loop][0] == "0":
                sign_bit = 1
            else:
                sign_bit = -1
            decimal_value = 0
            for decimal_loop in range(Q + R, 0, -1):
                if Q1[j_loop][i_loop][(Q + R) - decimal_loop + 1] == "1":
                    decimal_value = decimal_value + 2 ** (decimal_loop - Q - 1)
                else:
                    decimal_value = decimal_value + 0
            d1[j_loop, i_loop] = sign_bit * decimal_value
    if column_size == 1:
        d1 = d1.reshape([row_size])
    return d1


def real_to_decimal(weights, Q=4, R=0):
    d1 = weights
    dim_array = d1.ndim
    if dim_array == 1:
        row_size = d1.shape
        d1 = d1.reshape([row_size[0], 1])
    [row_size, column_size] = d1.shape
    decimal = np.zeros([row_size, column_size])
    Q1 = [["0" for i in range(column_size)] for j in range(row_size)]
    integ_d1 = np.floor(abs(d1))
    fract_d1 = abs(d1) - integ_d1
    for j_loop in range(0, row_size):
        for i_loop in range(0, column_size):
            if d1[j_loop, i_loop] < 0:
                Q1[j_loop][i_loop] = "1"
            if d1[j_loop, i_loop] <= (1):
                Q1[j_loop][i_loop] += np.binary_repr(
                    int(fract_d1[j_loop, i_loop] * 2**Q), width=Q
                )
            else:
                print("XXXXXX")  # should not happen??
                Q1[j_loop][i_loop] += np.binary_repr(int(0.875 * 2**Q), width=R + Q)
            decimal[j_loop, i_loop] = int(Q1[j_loop][i_loop], base=2)
    return decimal


def decimal_to_real(weights_bin, Q=4, R=0):
    # make sure weights_bin has 2 dimn with the second dim as 1.
    weights_bin = weights_bin.astype(int)
    weights_bin
    print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxx")
    row_size, column_size = weights_bin.shape
    Q1 = []
    for i_loop in range(0, row_size):
        Q1.append(np.binary_repr(weights_bin[i_loop, 0], width=Q + R + 1))
    d1 = np.zeros([row_size, 1])
    for j_loop in range(0, row_size):
        if Q1[j_loop][0] == "0":
            sign_bit = 1
        else:
            sign_bit = -1
        decimal_value = 0
        for decimal_loop in range(Q + R, 0, -1):
            if Q1[j_loop][(Q + R) - decimal_loop + 1] == "1":
                decimal_value = decimal_value + 2 ** (decimal_loop - Q - 1)
            else:
                decimal_value = decimal_value + 0
        d1[j_loop, 0] = sign_bit * decimal_value
    return d1


Model_para_inputs = sio.loadmat("Model_para_inputs.mat")
Layer1_weights = Model_para_inputs["Layer1_weights"]
Layer1_bias = Model_para_inputs["Layer1_bias"]

# For the demonstrating the use I am only quantizing weights and biases from only the first neuron


input_test = Model_para_inputs["image_test"]
input_test_decimal = np.reshape(
    real_to_decimal(
        binary_to_real(
            real_to_binary(np.concatenate([input_test[0, :], np.array([1.0])]))
        )
    ),
    [-1],
)
# Note there is a 1 added at the end of the input to enable multiplication of the bias with an input of 1.
input_file = open("inputsreal_2.txt", "w")
a = np.where(input_test_decimal != 0)
input_non_zero = input_test_decimal[a]
values = (np.concatenate([input_test[0, :], np.array([1.0])]))
print(values[a])
val2= values[a]
np.savetxt(input_file, val2, newline="\n", fmt="%.4f")

# print(np.sort(input_non_zero, axis=0))

input_file.close()


Neuron1_weights = Layer1_weights[:, 0]

Neuron1_weights_quant = binary_to_real(real_to_binary(Neuron1_weights))

# checking the quantization error
Quantization_error = Neuron1_weights - Neuron1_weights_quant
pyplot.plot(Quantization_error)
# pyplot.show()

Neuron1_bias = Layer1_bias[0]

Neuron1_bias_quant_decimal = real_to_decimal(
    binary_to_real(real_to_binary(Neuron1_bias))
)


Neuron1_weights_quant_decimal = real_to_decimal(Neuron1_weights_quant)
Neuron1_bias_quant_decimal = Neuron1_bias_quant_decimal[0]

parameters = np.concatenate(
    [np.reshape(Neuron1_weights_quant_decimal, [-1]), Neuron1_bias_quant_decimal]
)


parameters_nonzero = parameters[a]
weights_file = open("weightsreal_2.txt", "w")
np.savetxt(weights_file, (Neuron1_weights[a]), newline="\n", fmt="%.4f")

weights_file.close()
# print(np.sort(parameters, axis=0))
# print(np.sort(decimal_to_real(Neuron1_weights_quant_decimal[a]), axis=0))

# print(np.sort(Neuron1_weights, axis=0))
